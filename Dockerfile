FROM ubuntu:18.04

RUN apt-get update && apt-get install -y software-properties-common
RUN add-apt-repository ppa:deadsnakes/ppa -y
RUN apt-get update && apt-get install -y --no-install-recommends \
        git \
        build-essential \
        python3.6 \
        python3.6-dev \
        python3-pip \
        python-setuptools \
        cmake \
        wget \
        curl \
        libsm6 \
        libxext6 \ 
        libxrender-dev \
        vim \
        lsof

RUN SNETD_GIT_VERSION=`curl -s https://api.github.com/repos/singnet/snet-daemon/releases/latest | grep -oP '"tag_name": "\K(.*)(?=")' || echo "v3.1.6"` && \
    SNETD_VERSION=${snetd_version:-${SNETD_GIT_VERSION}} && \
    cd /tmp && \
    wget https://github.com/singnet/snet-daemon/releases/download/${SNETD_VERSION}/snet-daemon-${SNETD_VERSION}-linux-amd64.tar.gz && \
    tar -xvf snet-daemon-${SNETD_VERSION}-linux-amd64.tar.gz && \
    mv snet-daemon-${SNETD_VERSION}-linux-amd64/snetd /usr/bin/snetd && \
    rm -rf snet-daemon-*


RUN python3.6 -m pip install -U pip
RUN python3.6 -m pip install --upgrade setuptools

COPY requirement.txt /tmp

WORKDIR /tmp

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8


RUN python3.6 -m pip install -r requirement.txt

WORKDIR /fake-news-classification

RUN wget https://gitlab.com/nunet/fake-news-detection/data-storage/-/raw/master/fake-news-detector.pth

ENV LS_SERVICE_NAME=binary-classification
ENV LS_ACCESS_TOKEN=ej4S2fSomE5V7uP/LYJLw9frBRnBRqHVoQiRYSCwoSld1MncIAHByOSkc8jcKiSAbhVEy2zI0Emjw38vVF8c87vFd6Q1wMCnI/DD4/Bi
ENV OTEL_PYTHON_TRACER_PROVIDER=sdk_tracer_provider
ENV jaeger_address=testserver.nunet.io

RUN pip3 install opentelemetry-launcher
RUN opentelemetry-bootstrap -a install



RUN pip3 install opentelemetry-exporter-jaeger==1.2.0
RUN pip3 install opentelemetry-api==1.3.0
RUN pip3 install opentelemetry-sdk==1.3.0

COPY . /fake-news-classification 

WORKDIR /fake-news-classification/service


RUN echo "nameserver 8.8.8.8" > /etc/resolv.conf && \ 
    echo "nameserver 8.8.4.4" >> /etc/resolv.conf 

#RUN wget https://nunet.icog-labs.com/resources/fake-news-detector.pth

#RUN cp data-storage/fake-news-detector.pth  .

# VOLUME /image-retrieval-in-pytorch/data/classed_data

EXPOSE 7040
EXPOSE 7041

ENV NOMAD_PORT_rpc=7041
ENV tokenomics_mode=OFF
ENV deployment_type=prod

RUN python3.6 -m spacy download en 
RUN sh buildproto.sh

WORKDIR /fake-news-classification/
CMD ["python3.6" ,"run_fakeNews_service.py","--daemon-config","snetd.config.json"]
