import sys
import grpc
import time

import logging
from service.client import call

if __name__ == "__main__":
    try:
        head = 'Melania Trump cancels plans to attend Tuesday rally citing Covid recovery' 
        body = 'Melania Trump is canceling her first campaign appearance in months because she is not feeling well as she continues to recover '
        call(head,body)
    except Exception as e:
	    exit(1)

