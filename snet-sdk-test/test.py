# refer to https://pypi.org/project/snet.sdk/

import sys
sys.path.insert(0, 'client_libraries/nunet-org/fake-news-binary-classification/python/')


from snet import sdk
import fake_news_pb2_grpc
import snet
config = {
        "private_key": "0x3ede9d1ba003681131ffa970c981884d883fa8f905d417ceb3a6ee31538edc74",
        "eth_rpc_endpoint": "https://ropsten.infura.io/v3/03d8d698a7c444ce970ef743486bf608",
        "ipfs_rpc_endpoint": "http://ipfs.singularitynet.io:80"
}
from snet import sdk
snet_sdk = sdk.SnetSDK(config)
org_id = "nunet-org"
service_id = "fake-news-binary-classification"
service_client = snet_sdk.create_service_client(org_id, service_id,
                fake_news_pb2_grpc.fake_news_classificationStub,
                group_name='default_group',
                concurrent_calls=1)

import fake_news_pb2
request = fake_news_pb2.InputMessage(value='this is the tile : this is the body')
result = service_client.service.classify(request)
print(result) 