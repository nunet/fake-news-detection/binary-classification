
import grpc
from concurrent import futures
import time

import sys
sys.path.insert(0, 'service/')

from service_spec import fake_news_pb2
from service_spec import fake_news_pb2_grpc

from service_spec import service_proto_pb2_grpc
from service_spec import service_proto_pb2

import json

import test

import os
from resutils import *

from run_fakeNews_service import Log

import threading


from time import sleep

import opentelemetry
from opentelemetry import trace
from opentelemetry.instrumentation.grpc import GrpcInstrumentorServer
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.exporter.jaeger.thrift import JaegerExporter
from opentelemetry.sdk.trace.export import BatchSpanProcessor
from opentelemetry.trace import set_span_in_context
from opentelemetry.trace import NonRecordingSpan, SpanContext, TraceFlags


jaeger_address = os.environ['jaeger_address']


jaeger_exporter = JaegerExporter(
    agent_host_name=jaeger_address,
    agent_port=6831,
)


trace_provider=trace.get_tracer_provider().add_span_processor(
    BatchSpanProcessor(jaeger_exporter)
)

grpc_server_instrumentor = GrpcInstrumentorServer()
grpc_server_instrumentor.instrument()

tracer_server=opentelemetry.instrumentation.grpc.server_interceptor(tracer_provider=trace_provider)
tracer=trace.get_tracer(__name__)

logger=Log.logger

try:
    grpc_port = os.getenv("NOMAD_PORT_rpc")
except:
    grpc_port = "7011" 

try:
    tokenomics_mode=os.environ['tokenomics_mode']
except:
    tokenomics_mode=""


class fake_news_classificationServicer(fake_news_pb2_grpc.fake_news_classificationServicer):

    def classify(self, request, context):
        current_span = trace.get_current_span()
        current_span.set_attribute("http.route", "some_route")
        sleep(30 / 1000)

        tracer_info = eval(request.tracer_info)
        logger.info(tracer_info)
        span_id = tracer_info['span_id']
        trace_id = tracer_info['trace_id']

        span_context = SpanContext(
        trace_id=trace_id,
        span_id=span_id,
        is_remote=True,
        trace_flags=TraceFlags(0x01)
        )
        ctx = trace.set_span_in_context(NonRecordingSpan(span_context))
        with tracer.start_as_current_span("classify", context=ctx) as span:
            trace_info=span.get_span_context()
            span_id = trace_info.span_id
            trace_id = trace_info.trace_id
            tracer_info = {
                'trace_id': trace_id,
                'span_id': span_id
            }

            try:
                telemetry=resutils()
                start_time=time.time()
                cpu_start_time=telemetry.cpu_ticks()
            except Exception as e:
                exception = Exception(str(e))
                #span.record_exception(exception)
                #span.set_status(Status(StatusCode.ERROR, "error happened"))
                logger.error(e)
            resp = fake_news_pb2.OutputMessage()
            result = test.predict([request.headline+request.body])
            call_id = request.call_id
            span.add_event("event message", {"result": str(result)})
            span.add_event("event message", {"call id": str(call_id)})
            try:
                memory_used=telemetry.memory_usage()
                time_taken=time.time()-start_time
                cpu_used=telemetry.cpu_ticks()-cpu_start_time
                net_used=telemetry.block_in()
                result = {"binary_classification_result": result}
                resource_usage={'memory used':memory_used,'cpu used':cpu_used,'network used':net_used,'time_taken':time_taken}
                
                txn_hash=telemetry.call_telemetry(str(result),cpu_used,memory_used,net_used,time_taken,call_id,tracer_info)
                response=[str(result),str(txn_hash),str(resource_usage)]
                response=str(response)
                span.add_event("event message", {"response": str(response)})
                logger.info(response)
            except Exception as e:
                exception = Exception(str(e))
                #span.record_exception(exception)
                #span.set_status(Status(StatusCode.ERROR, "error happened"))            
                logger.error(e)
            logger.info(str(response))
            resp.response = response
            return resp

class GRPCproto(service_proto_pb2_grpc.ProtoDefnitionServicer):
    def req_service_price(self, req, ctxt):
        priceParams=service_proto_pb2.priceRespService()
        priceParams.cost_per_process=75
        priceParams.pubk="0xb5114121A51c6FfA04dBC73F26eDb7B6bfE2eB35"
        return priceParams

    def req_msg(self, req, ctxt):
        #TODO:  use https://googleapis.dev/python/protobuf/latest/ instead of reading from file 
        with open('service/service_spec/fake_news.proto', 'r') as file:
            proto_str = file.read()
        reqMessage=service_proto_pb2.reqMessage()
        reqMessage.proto_defnition=proto_str
        reqMessage.service_stub="fake_news_classificationStub"
        reqMessage.service_input="InputMessage"
        reqMessage.function_name="classify"
        reqMessage.service_input_params='["body","headline","call_id","tracer_info"]'
        return reqMessage
    
    def req_metadata(self, req, ctxt):
        #TODO:  use https://googleapis.dev/python/protobuf/latest/ instead of reading from file 
        with open('service/service_spec/fake_news.proto', 'r') as file:
                proto_str = file.read()
        tracer_info = eval(req.tracer_info)
        trace_id = tracer_info['trace_id']
        span_id = tracer_info['span_id']

        span_context = SpanContext(
        trace_id=trace_id,
        span_id=span_id,
        is_remote=True,
        trace_flags=TraceFlags(0x01)
        )
        ctx = trace.set_span_in_context(NonRecordingSpan(span_context))
        with tracer.start_as_current_span("req_metadata", context=ctx) as span:
            
            service_name=req.service_name
            
            respMetadata=service_proto_pb2.respMetadata()
            
            proto_defnition=proto_str
            service_stub="fake_news_classificationStub"
            service_input="InputMessage"
            function_name="classify"
            service_input_params='["body","headline","call_id","tracer_info"]'
            
            deployment_type=os.environ['deployment_type']
            if deployment_type=="prod":    
                with open('service/service_spec/service_definition_prod.json', 'r') as file:
                    service_definition_str = file.read()
            else:
                with open('service/service_spec/service_definition.json', 'r') as file:
                    service_definition_str = file.read()
            
            service_definition=json.loads(service_definition_str)
            service_definition["declarations"]["protobuf_definition"]=proto_defnition
            service_definition["declarations"]["service_stub"]=service_stub
            service_definition["declarations"]["function"]=function_name
            service_definition["declarations"]["input"]=service_input
            service_definition["declarations"]["service_input_params"]=service_input_params
            respMetadata.service_definition=json.dumps(service_definition)
            return respMetadata

def run_server(tf_session):
    class HTTPapi(BaseHTTPRequestHandler):
        def do_POST(self):
            content_length = int(self.headers['Content-Length'])
            content_type = self.headers['Content-Type']
            if content_type == 'application/json':
                data = self.rfile.read(content_length)
                json_data = json.loads(data.decode('utf-8'))
                if list(json_data.keys()).count('value') == 1:
                    input_data = {'value' : json_data['value']}


                    #TODO : add what ever is nessesery 
                    self.send_response(200)
                    self.send_header('Content-Type', 'text/plain')
                    self.end_headers()
                    self.wfile.write(json.dumps(dict(labeled_pred)).encode('utf-8'))
                else:
                    self.send_response(400)
                    self.send_header('Content-Type', 'text/plain')
                    self.end_headers()
                    self.wfile.write("Input should be json with keys 'headline' and 'body'")
            else:
                self.send_response(400)
                self.send_header('Content-Type', 'text/plain')
                self.end_headers()
                self.wfile.write("Input should be json with keys 'headline' and 'body'")
    return HTTPapi


server = grpc.server(futures.ThreadPoolExecutor(max_workers=100))
fake_news_pb2_grpc.add_fake_news_classificationServicer_to_server(fake_news_classificationServicer(), server)
service_proto_pb2_grpc.add_ProtoDefnitionServicer_to_server(GRPCproto(), server)

print('Starting server. Listening on port '+ str(grpc_port))
server.add_insecure_port('0.0.0.0:' + str(grpc_port))
server.start()

try:
    while True:
        time.sleep(86400)
except KeyboardInterrupt:
    server.stop(0)
