import grpc

import sys

sys.path.insert(0, 'service/')

from service_spec import fake_news_pb2
from service_spec import fake_news_pb2_grpc

def call(headline='this is sample head',body='this is sample body'):
    channel = grpc.insecure_channel('demo.nunet.io:7041')
    stub = fake_news_pb2_grpc.fake_news_classificationStub(channel)
    input_text = fake_news_pb2.InputMessage(headline=headline,body=body,call_id="1")
    response = stub.classify(input_text)
    return response


print(call())
