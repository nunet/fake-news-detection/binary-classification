# Fake News Binary Classification

## Description
The binary classification model uses a 30k fake news article dataset from the Kaggle competition [Fake and real news dataset Classifying the news]. Uses a BERT model with a single linear classification layer pre-trained with [Classifying the news] Kaggle competition fake and real news dataset. The model outputs 1 if an article is classified as fake news and 0 if not-fake.


Binary classification for fake news detection trained on 20k dataset


## Setup
----------------------------
```
pip install -r requirement.txt
cd service
sh buildproto.sh

#this will start the service
python server.py

#on other terminal 
test_binary_classification.py
```





## Build docker 
-------------------

```
# build the docker
docker build -t fake-news-classification:latest .

# run the docker
docker run -it -p 7041:7041 -p 7040:7040 fake-news-classification:latest
```

## call the service
--------------------------
```
snet --print-traceback client call nunet-org-new binary-classification default_group classify '{"headline":"news_headline","body":"news_body"}'
```

## deploy the daemon
---------------------------------------
use [this](https://docs.google.com/document/d/1docYOhSXCvRNWh9XEjQIk8PmRKTvG7NeiT4zKGKCwJ4/edit?usp=sharing) document to see how the publishing was done.
